/* vim:set ft=cpp ts=2 sw=2 sts=2 tw=80 cindent: */
#ifndef SOCKETS_LIB
#define SOCKETS_LIB

#include <winsock.h>
// Need to link with Ws2_32.lib
#pragma comment(lib, "ws2_32.lib")

class SockCom {
	private:
		WSADATA wsaStartupInfo;
		void cleanup();

		struct hostent *hostInfo; // Pointer to Host Information Entry
		struct servent *servInfo; // Pointer to Service Information Entry
		struct protoent *protoInfo; // Pointer to Protocol Information Entry

		struct sockaddr_in serverInfo; // Server Information
		struct sockaddr_in clientInfo; // Client Information

	protected:
		SockCom(const char *host, const char *port, const char *transport);

	public:
		~SockCom();
		SOCKET sockDesc;
		virtual int send(char *buffer);
		virtual int send(char *buffer,int sendLength);
		virtual int recv(char *buffer, int *bufferSize);

		const sockaddr_in* getServerInformationStructAddr() const;
		SOCKET getSocketDescriptor() const;

		static const int serverStructLength = sizeof(struct sockaddr_in);

		void socketBind();
		
};

class UDPSocket : public SockCom {

	private:
		void openSocket();

	public:
		UDPSocket(const char *host, const char *port) : SockCom(host, port, "udp") { openSocket(); };
		virtual int send(char *buffer);
		virtual int send(char *buffer, int sendLength);
		virtual int recv(char *buffer, int *bufferSize);

};

void SockCom::socketBind() {
	if( bind(sockDesc, (struct sockaddr *)getServerInformationStructAddr(), sizeof(struct sockaddr_in)) < 0 ) {
	}
}
int SockCom::send(char* buffer) {
	cerr << "Not implemented, should use child's, send char buff" <<endl;
	return -1;
}

int SockCom::send(char* buffer,int sendLength) {
	cerr << "Not implemented, should use child's, send char buff, length" <<endl;
	return -1;
}

int SockCom::recv(char* buffer, int* bufferSize) {
	cerr << "Not implemented, should use child's, recv char buff , size" <<endl;
	return -1;
}

SockCom::SockCom(const char *host, const char *port, const char *transport) {
#ifdef WIN32
	/* WSAStartup is needed with the Windows operating systems because the system
		 uses dynamically linked libraries (DLLs). When a program calls WSAStartup,
		 the system searches for an appropriate library and binds to it.
		 WSAStartup has two arguments
		 - the version number in HEX
		 - WSADATA structure into which the version information is written
	*/
	/* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
	WORD wVersionRequested = MAKEWORD(2, 2);
	if (WSAStartup(wVersionRequested, &wsaStartupInfo) != 0) {
		cerr << "Could not open Windows DLL." << endl;
		exit(0);
	}
#endif //WIN32

	// Clear Server/Client Information Structure
	memset((void *)&serverInfo, '\0', sizeof(struct sockaddr_in));
	memset((void *)&clientInfo, '\0', sizeof(struct sockaddr_in));
	serverInfo.sin_family = AF_INET;
	clientInfo.sin_family = AF_INET;

	/* Define Port in the Server Information Header */
	if ( servInfo = getservbyname(port, transport) ) {
		serverInfo.sin_port = servInfo->s_port;
	} else if ( (serverInfo.sin_port = htons((u_short)atoi(port))) == 0 ) {
		// Unable to find port information
		cerr << "Unable to define port information..." << endl;
		cleanup();
		exit(1);
	}

	/* Define Host in the Server Information Header */
	if ( hostInfo = gethostbyname(host) ) {
		memcpy(&serverInfo.sin_addr, hostInfo->h_addr, hostInfo->h_length);
	} else if ( (serverInfo.sin_addr.s_addr = inet_addr(host)) == INADDR_NONE ) {
		// Unable to set host information
		cerr << "Unable to define host information..." << endl;
		cleanup();
		exit(1);
	}


}

/* SockCom Destructor
 * Cleans up the socket descriptor and all socket dependencies
 */
SockCom::~SockCom() {
	cleanup();
}

const sockaddr_in* SockCom::getServerInformationStructAddr() const {
	return &serverInfo;
}

SOCKET SockCom::getSocketDescriptor() const {
	return sockDesc;
}

/* Function: cleanup
 * SockCom Helper function
 * Closes the socket handle and releases all required DLLs.
 */
void SockCom::cleanup(){
	// Terminate communication and deallocate a descriptor
	closesocket(sockDesc);
#ifdef WIN32
	// Terminate the use of the socket library
	WSACleanup();
#endif //WIN32
	//cout << "Socket has been destroyed and DLLs released." << endl;

}

/* Function: openDatagramSocket
 * Calls socket to create a new socket that can be used for network
 * communication. The function assigns the descriptor for the newly created
 * datagram socket.
 */
void UDPSocket::openSocket() {
	// Calls socket to allocate socket descriptor
	sockDesc = socket(PF_INET, SOCK_DGRAM, 0);

	if (sockDesc == INVALID_SOCKET) {
		cerr << "Could not create socket" << endl;
		WSACleanup();
		exit(0);
	}

}

int UDPSocket::send(char *buffer){
#ifdef LOGGING
	logToFile(logFile, "Send buff contains:\n%s\n", buffer);
#endif

	//cout << "Sending Message" << endl;
	//cout << buffer << endl;
	int retcode = sendto(getSocketDescriptor(), buffer, strlen(buffer), 0,
									(struct sockaddr *) getServerInformationStructAddr(), serverStructLength);

	return retcode;
}

int UDPSocket::send(char *buffer, int sendLength){
	/* retcode = sendto(s, message, len, flags, toaddr, toaddrlen);
		 s -> an unconnected socket
		 message -> the address of a buffer that contains the data to be sent
		 len -> specified the number of bytes in the buffer
		 flags -> specified debugging or control options
		 toaddr -> a pointer to a sockaddr_in struct that contains the end point
		 address
		 toaddrlen -> specifies the length of the address struct
	*/

	cout << "Sending Message" << endl;
	int retcode = sendto(getSocketDescriptor(), buffer, sendLength, 0,
									(struct sockaddr *) getServerInformationStructAddr(), serverStructLength);

	return retcode;
}

/* :: UDPSocket Function :: recv
	 ComSock API abstraction for receiving messages from specified client, given a
	 connectionless udp datagram communication channel was desired.
 */

int UDPSocket::recv(char *buffer, int *bufferSize){
	/* retcode = recvfrom(s, buf, len, flags, from, fromlen);
		 s -> socket to use
		 buf -> specifies the address of a buffer into which the system will place
		 the next datagram
		 len -> specifies the space available in the buffer
		 from -> specifies a second buffer into which the system will place the
		 source address
		 fromlen -> specifies the address of an integer, the length of the from
		 buffer, then contains the length of the source address the system placed in
		 the buffer
	*/
	//cout << "Receiving Message" << endl;
	int retcode = recvfrom(getSocketDescriptor(), buffer, *bufferSize, 0, nullptr, nullptr);
#ifdef LOGGING
	if (retcode == SOCKET_ERROR) {
		logToFile(logFile, "recvfrom: error number %d\n", GetLastError());
	}
#endif
	return retcode;
}

#endif //SOCKETS_LIB
