/* vim:set ft=cpp ts=2 sw=2 sts=2 tw=80 cindent: */
#ifndef LOGGING
#define LOGGING
#include <stdarg.h>

/* 
 */
void logToFile(FILE * stream, const char *format, ...) {
	time_t currTime;
	time(&currTime);
	fprintf(stream, "%d ", currTime);
	va_list args;
	va_start(args, format);
	vfprintf(stream, format, args);
	va_end(args);
}
#endif //LOGGING
