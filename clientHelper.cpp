/* vim:set ft=cpp ts=2 sw=2 sts=2 tw=80 cindent: */
#ifndef CLIENT_HELPER
#define CLIENT_HELPER

class ClientInterface {
	private:
		static const char spaceDelim = ' ';
		IMClient* myClient;
		void help();
		/* Functions to Help Determine User Input Type
		 */
		bool isLogin(string*);
		bool isSend(string*);
		bool isList(string*);
		bool isDownload(string*);
		bool isQuit(string*);
		bool isHelp(string*);
		bool isCustom(string*);
		bool isFileNext(string*);
		bool isFileList(string*);

		void loginState(string*);
		void sendState(string *);
		void listState();
		void downloadFile(string *);
		void customMessage(string *);

	public:
		ClientInterface(IMClient* client);
		void promptUser();
};

ClientInterface::ClientInterface(IMClient* client) : myClient(client) {
#ifdef LOGGING
		logToFile(logFile, "Started Interface for User Interaction...\n\n");
#endif
}

void ClientInterface::promptUser() {
	string userInput;
	string s_login("login"), s_send("send"), s_list("list"),
				 s_download("download"), s_quit("quit");
	cout << "Waiting for user input, type <help> for more information..." <<
		endl;
	while (true) {
		getline(cin, userInput);
		if (isLogin(&userInput)) {
			//cout << "user entered login state" <<endl;
			loginState(&userInput);
		} else if (isHelp(&userInput)) {
			help();
		} else if (!myClient->getLoginState()) {
			cout << "You must login before you can use client functions." << endl;
		} else if (isSend(&userInput)) {
			//cout << "Enter Send state" << endl;
			sendState(&userInput);
		} else if (isList(&userInput)) {
			cout << "Enter File List state" << endl;
			listState();
		} else if (isDownload(&userInput)) {
			cout << "Enter Download state" << endl;
			downloadFile(&userInput);
		} else if (isCustom(&userInput)) {
			cout << "Input Custom Message..." << endl;
			customMessage(&userInput);
		} else if (isFileList(&userInput)) {
			cout << "Current Downloads" << endl;
			myClient->listCurrentFileTransfers();
		} else if (isFileNext(&userInput)) {
			cout << "Downloading Stuck Files" << endl;
			myClient->requestStuckFileTransfer();
		} else if (isQuit(&userInput)) {
			cout << "Quiting..." << endl;
			myClient->logoff();
			break;
		} else {
			cout << "You entered: " << userInput << endl;
			help();
		}
	}
}

void ClientInterface::downloadFile(string* input) {
	string fileName;
	int beginName = (*input).find(spaceDelim) + 1;
	fileName = (*input).substr(beginName, string::npos);
	if (fileName.compare(0,fileName.length(), "download") == 0) {
		cout << "need to specify file name with download command" << endl;
		cout << "Please enter a file name now: " ;
		getline(cin, fileName);
	}
	//char *name = _strdup(fileName.c_str());
	cout << "Specified file name: " << fileName << endl;
	myClient->downloadFile(&fileName);
	//free(name);
}

void ClientInterface::listState() {
	myClient->fileList();
}

void ClientInterface::loginState(string* input) {
	string userName;
	int beginName = (*input).find(spaceDelim) + 1;
	userName = (*input).substr(beginName, string::npos);
	if (userName.compare(0,userName.length(), "login") == 0) {
		cout << "need to specify user name with login command" << endl;
		cout << "Please enter a user name now: " ;
		getline(cin, userName);
	}
	//cout << "Login user name is " << userName << endl;
	myClient->setUser(&userName);
	myClient->login();
}

void ClientInterface::sendState(string* input) {
	string buddyName;
	int beginName = (*input).find(spaceDelim) + 1;
	buddyName = (*input).substr(beginName, string::npos);
	if (buddyName.compare(0,buddyName.length(), "send") == 0) {
		cout << "need to specify buddy's id with send command" << endl;
		cout << "Please enter a buddy's userid now: " ;
		getline(cin, buddyName);
	}

	// Check if name is valid ( if user is logged in )

	// Ask user to input message
	string message;
	cout << "Please type the message you wish to send to " << buddyName << endl <<
	 "Press <enter> to terminate input." << endl;
	getline(cin, message);

	// Send message to server
	myClient->sendMessage(&buddyName, &message);
}

void ClientInterface::customMessage(string* input) {
	// Ask user to input message
	string message;
	getline(cin, message);
	// Send message to server
	myClient->sendCustom(&message);
}

bool ClientInterface::isLogin(string* input) {
	static const string s_login = "login";
	static const int len = strlen(s_login.c_str());
	return strncmp((*input).c_str(), s_login.c_str(),len) ? 0 : 1;
}

bool ClientInterface::isFileNext(string* input) {
	static const string s_file = "file";
	static const int len = strlen(s_file.c_str());
	return strncmp((*input).c_str(), s_file.c_str(),len) ? 0 : 1;
}

bool ClientInterface::isFileList(string* input) {
	static const string s_file = "file";
	static const string s_list = "list";
	int foundFile = (*input).find(s_file);
	int foundList = (*input).find(s_list);
	return ((foundFile != string::npos) && (foundList != string::npos) )? 1 : 0;
}

bool ClientInterface::isSend(string* input) {
	static const string s_send = "send";
	static const int len = s_send.length();
	return (*input).compare(0,len, s_send) ? 0 : 1;
}

bool ClientInterface::isList(string* input) {
	static const string s_list = "list";
	static const int len = s_list.length();
	return input->compare(0,len, s_list) ? 0 : 1;
}

bool ClientInterface::isDownload(string* input) {
	static const string s_down = "download";
	static const int len = s_down.length();
	return (*input).compare(0,len, s_down) ? 0 : 1;
}

bool ClientInterface::isQuit(string* input) {
	static const string s_quit = "quit";
	static const int len = s_quit.length();
	return input->compare(0,len, s_quit) ? 0 : 1;
}

bool ClientInterface::isHelp(string* input) {
	static const string s_help = "help";
	static const int len = s_help.length();
	return input->compare(0,len, s_help) ? 0 : 1;
}

bool ClientInterface::isCustom(string* input) {
	static const string s_help = "custom";
	static const int len = s_help.length();
	return input->compare(0,len, s_help) ? 0 : 1;
}

void ClientInterface::help() {
	cout << "The following is a list of commands that can be used..." << endl << endl
		<< "login <userName> - login to server" << endl
		<< "send <buddyName> - starts a message to the specified user" << endl
		<< "list - shows a list of files available to download" << endl
		<< "file - request waiting files (use if transfer is stuck)" << endl
		<< "file list - list currently downloading files" << endl
		<< "download <file> - download the specified file" << endl
		<< "quit - logoff and exit the program" <<endl
		<< "help - shows this dialogue" << endl << endl;
}

#endif
