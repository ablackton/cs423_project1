/* vim:set ft=cpp ts=2 sw=2 sts=2 tw=80 cindent:
 * File Name: cs423_blackton_project1.cpp
 * Program Author: Andrew Blackton
 * CS 423 Client/Server Programming :: Professor Bob Cotter
 * Project #1 Client Programming (Connectionless Client)
 * Due Date: March 05, 2013
 * 
 * A simplified Instant Messaging program. This is an UDP implementation, using
 * a server-based model in which we have a centralized server that handles all
 * communications between clients. When a client signs onto the system, they get
 * a list of all of the users who are currently signed onto the system. When a
 * user wants to send a message, they will send it to the server which will
 * forward it to the desired recipient.
 * 
 * When someone else logs onto the system, the server will notify you. Likewise
 * you will get a notification when someone leaves. Since the centralized server
 * will be periodically sending these messages to all users, your IM client will
 * need to maintain a separate thread to monitor the ``connection'' with the
 * server. At the same time, your client will need to maintain a prescence with
 * the user of the program, so that the user can request to either send a
 * message, check for received messages, or to log off.
 *
 * This is a TRUE connectionless UDP client for an Instant Messaging Service.
 */

#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <ctime>
#include <iostream>
#include <process.h>

using namespace std;

const char *defaultServer = "134.193.128.197";
const char *defaultPort = "3456";


#include "logging.cpp"
#ifdef LOGGING
	FILE * logFile;
#endif
#include "imClient.cpp"
#include "clientHelper.cpp"

void programHelp(char *argv[]);

int main(int argc, char *argv[]) {
#ifdef LOGGING
	if ( fopen_s(&logFile, "log.txt", "a+") != 0 ) {
		cerr << "Unable to open log file" << endl;
		exit(1);
	}
	logToFile(logFile, "Starting IM Client...\n");
#endif

	char *host;
	char *port;
	memcpy(&port, &defaultPort, sizeof(defaultPort));
	// Parse Command Line Arguments: hostname/ip and port/service
	switch (argc) {
		case 1:
			memcpy(&host, &defaultServer, sizeof(defaultServer));
			break;
		case 3:
			port = argv[2];	
		case 2:
			host = argv[1];
			break;
		default:
			programHelp(argv);
	}

	// Start IMClient which will spawn a socket for network communications
	IMClient *hello = new IMClient(host, port);

	// Pass IMClient into ClientInterface for user interactive session
	ClientInterface* client = new ClientInterface(hello);

	// Start the ClientInterface user prompt
	client->promptUser();

	//string temp;
	//getline(cin, temp);

	delete client;
	delete hello;
#ifdef LOGGING
		logToFile(logFile, "Exiting IM Client...\n\n");
		fclose(logFile);
#endif
	return 0;
}

/* Function prints program usage message, then exits
 */
void programHelp(char *argv[]) {
	cout << "usage: " << argv[0] << " [host [port]]" << endl;
	exit(1);
}
