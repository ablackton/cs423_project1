/* vim:set ft=cpp ts=2 sw=2 sts=2 tw=80 cindent: */
#ifndef IM_CLIENT
#define IM_CLIENT

#include "sockets.cpp"
#include <map>

#define PROTO_UDP
#define MESSAGE_NUM_LENGTH 5
#define MAX_USER_LEN 10
#define SEND_MESSAGE_SIZE 513
#define RECV_MESSAGE_SIZE 530

unsigned __stdcall startServerHandlerInThread(void* arg);

class IMClient{
	private:
		// Class Constants, used primarily to remove magic variables from class
		// functions
		static const char c_semiDelim = ';';
		static const int i_ackOffset = 4;

		class UsersList {
			private:
				typedef map<string, int> UserMap;
				UserMap userList;
				int numberOfUsers_;

			public:
				UsersList() : numberOfUsers_(0) { };

				void addUser(string myUser) {
					userList[myUser] = numberOfUsers_++;
					cout << "User added: " << myUser << endl;
				};

				void delUser(string *userName) {
					UserMap::iterator it_user =  userList.find(*userName);
					userList.erase(it_user);
					cout << "User removed: "<< userName << endl;
				};

				bool isLoggedOn(string *userName) {
					UserMap::const_iterator user =  userList.find(*userName);
					return (user == userList.end()) ? 0 : 1;
				};

				bool isLoggedOff(string *userName) {
					UserMap::const_iterator user =  userList.find(*userName);
					return (user == userList.end()) ? 1 : 0;
				};

				void printUsers() {
					for (UserMap::iterator it_user = userList.begin(); it_user !=
							userList.end() ; ++it_user) {
						cout << it_user->first << endl;
					}
				};
		};

		/* Define Message Type (t_message)
			 Used for Identifying the type of actions that should be associated with a
			 specific server message.  */
		enum t_message {
			type1, /*Login */
			type2, /*Send Message*/
			type3, /*Sign off*/
			type4, /*List Files*/
			type5, /*Get File*/
			error  /*Error*/
		};

		/* Define Server Response Types
		   Not currently used and won't be; so delete me!!! */
		enum t_serverMessageTypes {
			ack_login, /*Response from type1 message*/
			ack_goodbye, /*Response from type3 message*/
			ack_fileCount, /*Response from type4 message*/
			message_from, /*Message Received Notification*/
			file, /*File Download in progress, response from type5 message*/
			r_error /*Catchall, not specified handle errors*/
		};

		/* File Information Structure (FileInfo)
			 Used in the MessageInfo structure when a type 5 message is sent.
			 Facilitates downloading files from the server, by keeping track of all
			 the important variables.
			 */
		struct FileInfo {
			FILE* outputFile; // File Handle for file dump from server
			int		blockCount; // Current download block count
			int		blockCountChk;
			string	fileName; // Name of file being downloaded

			/* FileInfo Constructor
				 Requires a name. Called with a type5 message from the client. Opens
				 file handle and sets variables to prepare for file download.
				 */
			FileInfo(string *name) : fileName(*name), blockCount(1), blockCountChk(0) {
				if ( fopen_s(&outputFile, name->c_str(), "w") != 0 ) {
					cerr << "unable to create the specified file" << endl;
				}
#ifdef LOGGING
			logToFile(logFile, "Opening File handle for %s; Block Count at 0\n", name);
#endif
			};

			/* FileInfo Destructor
				 Closes open file handle and otherwise performs normal c++ memory
				 cleanup actions. Call with delete to free memory assigned for structure.
				 */
			~FileInfo() {
				fclose(outputFile); // Close open file handle for downloaded file
			};
			// Helper functions, used for downloading files and error handling
			void incrementBlockCount() { ++blockCount; };
			void updateBlockCount(int newNum) { blockCount = newNum; };
		};

		/* Message Information Structure (MessageInfo)
			 Used to keep track of a messages attributes. Held in the
			 waitingForResponse structure. When a particular message is sent its
			 message information is associated with the msg#. When a message is
			 received it can be identified and various attributes can be accessed
			 through this structure.
			 */
		struct MessageInfo {
			/* Origional Message Number, assigned by client when message is sent */
			int	messageNum; 
			t_message messageType; // Message Characteristics are associated w/ message type
			FileInfo* fileInfo; // FileInfo Object (created when requesting a file download)

			/* MessageInfo Contructor: assigns message attributes
				mn: client's message number at time of sending message
				mt: message type, defines how server responses should be handled
				*/
			MessageInfo(int mn, t_message mt) : messageNum(mn), messageType(mt), fileInfo(nullptr) {};

			/* MessageInfo Contructor: assigns message attributes used for type5 messages
				mn: client's message number at time of sending message
				mt: message type, defines how server responses should be handled
				name: file name, associated with the requested file name
				*/
			MessageInfo(int mn, t_message mt, string* name) : messageNum(mn), messageType(mt) {
				fileInfo = new FileInfo(name);
			};
			// Simple Helper Function to monitor and modify structures properties
			t_message getMessageType() const { return messageType; };
			void updateMsgNum(int newNum) { messageNum = newNum; };
			int getMsgNum() { return messageNum; };
		};

		// Define type MessageList structure used to hold message information
		typedef map<int, MessageInfo*> MessageList;

		SockCom* mySock; // Communication Socket for imClient, used for communication
		int msgNumInt; // Current Message Number of Client, used when sending new messages to server
		char msgNumAsc[MESSAGE_NUM_LENGTH+1]; /* Message Number in ascii format; not
																						 really necessary, but is here in
																						 case it needs to be used; set first
																						 time by incrementMessageNum, and
																						 updated by the function */

		char userName[MAX_USER_LEN+1]; // User's login name, has set length defined
		char sendBuffer[SEND_MESSAGE_SIZE]; /* Send buffer used by client to store
																					 information being sent to server
																					 through Communication Socket */

		void incrementMessageNum(); // Increment Client's Current Message Number
		void setMessageNum(int); // Set the Client's Current Message Number; used for error handling

		void sendBuff(char* buff); /* Helper function send specified information
																	to server, automatically increments message
																	number; use with caution can be dangerous if
																	used with the wrong message type; should never be
																	used in response to a server message  */

		MessageList waitingForResponse; /* Structure holds information related to
																			 send messages that are waiting for server
																			 feedback */

		/* Add message to message list used by client to handle incoming messages from server */
		void addToWaitList(int mn, t_message mt); 
		/* Added message to message list, used for file transfer; called when
		 * sending type5 messages to server 
		**/
		void addToWaitList(int mn, t_message mt, string*); 

		/* Process Handle for thread, created when serverHandler thread is created;
		   may be used in the feature to get return values from the thread;
		   currently doesn't check for graceful shutdown */
		HANDLE h_serverHandler;

		bool loginState; // used by client interface to lock out interface until user logs into server
		void setLoginState(); // Sets loginState variable to true; truly useful function

		bool recvState; /* set at class creation, used to break out of serverHandler
											 loop when user wants to quit, set to false upon user logout message */


		void unsetRecvState(); /* Helper function to set recvState to false, used by
															class destructor */

		/* Determines type of message received from server and facilitates processing of the message */
		void processServerMessage(char *); 
		/* Server Error Message Handler Function */
		void processErrorMessage(char *); 
		/* Handles display of message from another user in the group */
		void processIncommingMessage(char*); 
		void ackHandler(char*);
		void buffToFile(char*);

		/* Helper Functions Used to Determine Incoming Message Type */
		// TODO: change to const char *; sorry my programming standards were a bit
		// sloppy for this project :*(
		bool isIncommingMsg(char*) const;
		bool isFile(char*) const;
		bool isAck(char*) const;
		bool isError(char*) const;

		// Given the message number from a server message determine what type of
		// message the server was responding to by looking through the MessageList
		t_message getAckMessageType(int);

		UsersList usersAvail; // List of Users available to the client
		/* Function that deals with type1 server response ack message, to collect
		   list of available users */
		void setUsersFromWelcome(char*);


	public:
		IMClient(const char *host, const char *service);
		~IMClient();

		// Initialize the IMClient's user
		void setUser(); // Request User to Interactively input user name
		void setUser(string* name); // Given string set user name
		void setUser(char* name); // Given c_string set user name

		/* IM Client API functions used to interact with the IMClient Class */
		void login(); //type 1 
		void sendMessage(string* sendTo, string* message); //type 2
		void sendMessage(char* sendTo, char* message); //type 2
		void logoff(); //type 3
		void fileList(); //type 4
		void downloadFile(string*); //type 5

		/* Various Helper Functions for Debugging and Problem Resolution */
		void sendCustom(string *); /* Send user's custom message (doesn't not increment message count */
		void requestStuckFileTransfer(); /* Goes through finds any stuck file transfers and requests message from server*/
		void listCurrentFileTransfers(); /* List status of current file transfers */

		bool getLoginState() const; // Returns current login state

		void serverHandler(); // Function to handle incoming server messages, called in seperate thread
};

/* :: IMClient Function :: getAckMessageType
	 given the specified message number return the type of message that was
	 originally sent to the server and to which the server is responding.
 */
IMClient::t_message IMClient::getAckMessageType(int msgNum) {
	MessageList::iterator msgInfo;
	msgInfo = waitingForResponse.find(msgNum);
	return msgInfo->second->getMessageType();
}

/* :: IMClient Function :: setLoginState
	 Set Client's login state to true; doesn't wait till ack
	 Lets the client helper access user functions
 */
void IMClient::setLoginState() {
	loginState = true;
}

/* :: IMClient Function :: unsetRecvState
	 Unset recvState boolean; stops the client's server listener from attaching to
	 the socket again. After it receives its final message.
 */
void IMClient::unsetRecvState() {
	recvState = false;
}

/* :: IMClient Function :: getLoginState
	 Returns the value of the loginState variable; used by the client helper class
	 to determine if the user should be permitted to enter commands other than
	 login
 */
bool IMClient::getLoginState() const {
	return loginState;
}

/* :: IMClient Function :: 
	 Add user's server request to list of messages waiting for response.
 */
void IMClient::addToWaitList(int mn, t_message mt) {
	waitingForResponse[mn] = new MessageInfo(mn, mt);
}

/* :: IMClient Function :: addToWaitList (type5)
	Add user's server request to list of messages waiting for response
	This function is overloaded for a type5 message; it will also open a
	filehandle for the requested file.
 */
void IMClient::addToWaitList(int mn, t_message mt, string *name) {
	waitingForResponse[mn] = new MessageInfo(mn, mt, name);
}

/* :: IMClient Function :: 
	 Prompt the user for a username and set it to that value
 */
void IMClient::setUser() {
	cout << "Input your user handle: " ;
	cin.getline(userName, MAX_USER_LEN);
}

/* :: IMClient Function :: 
	 set username from given c string (null terminated character array)
 */
void IMClient::setUser(char* user) {
	memset(userName, '\0', MAX_USER_LEN);
	sprintf_s(userName, "%s", user);
}

/* :: IMClient Function :: setUser
	 Set user name from given string value
 */
void IMClient::setUser(string* user) {
	memset(userName, '\0', MAX_USER_LEN);
	strncpy_s(userName, (*user).c_str(), (*user).length());
}

/* :: IMClient Function :: incrementMessageNum
	 Helper function that increments client's current message number by one
 */
void IMClient::incrementMessageNum() {
	sprintf_s(msgNumAsc, "%d", ++msgNumInt);
}

/* :: IMClient Function :: setMessageNum
	 Helper function; sets the clients message number to specified value
	 Used mainly for Error handling.
 */
void IMClient::setMessageNum(int _newNum) {
	msgNumInt = _newNum;
	sprintf_s(msgNumAsc, "%d", msgNumInt);
}

/* :: IMClient Function :: sendBuff
	 send user's message and increments message number
	 Should be use when client is requesting something from the server for the
	 first time, should not be used to handle replies.
 */
void IMClient::sendBuff(char* buff) {
	mySock->send(buff);
	incrementMessageNum();
}

/* :: IMClient Function :: ackHandler
	 handles server's responses for user requests
 */
void IMClient::ackHandler(char *buff) {
		MessageList::iterator msgInfo;
		//cerr << "Server Acknowledgement Message" << endl;
		// Will need to get rid of the magic numbers here
		// pos -> ack; 3+1
		// len -> size of msgNum 5
		char ackMsgNum[MESSAGE_NUM_LENGTH+1];
		// Extract Message number from receive buffer
		strncpy_s( ackMsgNum, buff+i_ackOffset, MESSAGE_NUM_LENGTH );
		ackMsgNum[MESSAGE_NUM_LENGTH] = '\0';
#ifdef LOGGING
		logToFile(logFile, "Received ACK from server for msg %s\n", ackMsgNum);
#endif
		int i_ackMsgNum = atoi(ackMsgNum);
		//cout << i_ackMsgNum << endl;

		// Depending on the type of message the server was responding to
		switch (getAckMessageType(i_ackMsgNum)) {
			case (type1):
				/*Login Acknowledgement*/
				cout << buff << endl;
				setUsersFromWelcome(buff);
				usersAvail.printUsers();
				break;
			case (type2):
				/*Send Message*/
				//cout << buff << endl;
				msgInfo = waitingForResponse.find(i_ackMsgNum);
				waitingForResponse.erase(msgInfo);
#ifdef LOGGING
				logToFile(logFile, "Removed Message for Waiting Queue %s\n", ackMsgNum);
#endif
				break;
			case (type3):
				/*Sign off*/
				cout << buff << endl;
				break;
			case (type4):
				/*List Files*/
				cout << buff << endl;
				break;
			default:	
				cerr << "We should not get here... Received Unknown ACK type" << endl;
				cout << buff << endl;
		}
}

/* :: IMClient Function :: processErrorMessage
	 Error message handling function; work in process currently deals the
	 following error messages:
	 - BlockCount mismatch
	 - Message number off
	 - FileRequest invalid
 */
void IMClient::processErrorMessage(char *buff) {
	char filMsgNum[MESSAGE_NUM_LENGTH+1];
	// Length of Error Error: string 6
	// Extract Message number from receive buffer
	strncpy_s( filMsgNum, buff+6, MESSAGE_NUM_LENGTH );
	filMsgNum[MESSAGE_NUM_LENGTH] = '\0';
	//cout << "Error for message #: " << filMsgNum << endl;
	int i_filMsgNum = atoi(filMsgNum);
	string s_buff = buff;
	MessageList::iterator msgInfo = waitingForResponse.find(i_filMsgNum);


	if (s_buff.find("BlockCount mismatch.") != string::npos) {
		// Correct Block Count Mismatch
		// Error:20406;BlockCount mismatch.  Expect 1480

		char filBlockCount[MESSAGE_NUM_LENGTH+1];
		strncpy_s( filBlockCount, buff+40+1, MESSAGE_NUM_LENGTH );
		filBlockCount[MESSAGE_NUM_LENGTH] = '\0';
		//cout << "Block count should be: " << filBlockCount << endl;
		int i_filBlockCount = atoi(filBlockCount);
		FileInfo *fileInfo = msgInfo->second->fileInfo;
		// Update Block count w/ Expected Value
		//cout << "Updating File Block Count" << endl;
		(*fileInfo).updateBlockCount(i_filBlockCount);

		// Send updated request
		memset(sendBuffer, '\0', SEND_MESSAGE_SIZE);
		sprintf_s(sendBuffer, "%s;6;%05d", filMsgNum, fileInfo->blockCount);
		mySock->send(sendBuffer);
		cout << "Block Mismatch Error, corrected." << endl;




	} else if (s_buff.find("Message number is off") != string::npos ) {
		// Correct Message Number is off error
		// Error:20408; Message number is off.  Was 20408; should be 20406

		// Delete Message from waiting queue malformed
		waitingForResponse.erase(msgInfo);

		// Correct User's Message Number
		char c_newMessageNum[MESSAGE_NUM_LENGTH+1];
		strncpy_s( c_newMessageNum, buff+58, MESSAGE_NUM_LENGTH );
		c_newMessageNum[MESSAGE_NUM_LENGTH] = '\0';
		setMessageNum(atoi(c_newMessageNum));
		cout << "Corrected Message # Error" << endl;




	} else if (s_buff.find("FileRequest invalid") != string::npos ) {
		//Error;20406;FileRequest invalid. File already open.
		cout << "Invalid File Operation. Sorry, download not started." << endl;
		waitingForResponse.erase(msgInfo);
	}
}

/* :: IMClient Function :: processIncommingMessage
	 Process message from another user and print it to the screen. Currently there
	 is no nice resource handling for this function. It doesn't check and wait to
	 see if the user is typing anything into the console before printing. Should
	 probably dump the message into a message queue to print upon user request or
	 have a separate thread running and orchestrates notifying the user of server
	 messages.
 */
void IMClient::processIncommingMessage(char *buff) {
	string s_buff(buff);
	string msgFrom;
	string messageContents;
	int i_startFrom = s_buff.find(" ");
	++i_startFrom;
	int i_endFrom = s_buff.find(" ", i_startFrom);
	msgFrom.assign(buff+i_startFrom, i_endFrom-i_startFrom);
	cout << "New Message from: " << msgFrom << endl;

	int i_msgStart = s_buff.find("\n");
	++i_msgStart;
	s_buff.erase(s_buff.begin(), s_buff.begin()+i_msgStart);

	cout << s_buff << endl;
#ifdef LOGGING
	//logToFile(logFile, "Received Message from Server\nMessage from: %s\nMessage is %s", msgFrom, s_buff);
#endif
}

/* :: IMClient Function :: processServerMessage
	 Handles various server message types and processes them accordingly
 */
void IMClient::processServerMessage(char* buff) {
	if (isAck(buff)) {
		ackHandler(buff);
	} else if (isFile(buff)) {
		//cerr << "File Transmission Message" << endl;
		buffToFile(buff);
	} else if (isIncommingMsg(buff)) {
		cerr << "Incomming Message from Server" << endl;
		processIncommingMessage(buff);
	} else if (isError(buff)) {
		cout << buff << endl;
		processErrorMessage(buff);
	} else {
		cerr << "Unrecognized incomming message" << endl;
		cout << buff << endl;
	}
}

/* :: IMClient Function :: buffToFile
	 Take fil message from server, associate it with a particular request, strip
	 header, and dump datagram into file.
		 fil;msg#;blockCount\n
		 512 bytes of data (except for last block).
 */
void IMClient::buffToFile(char *buff) {

	char filMsgNum[MESSAGE_NUM_LENGTH+1];
	// Extract Message number from receive buffer
	strncpy_s( filMsgNum, buff+i_ackOffset, MESSAGE_NUM_LENGTH );
	filMsgNum[MESSAGE_NUM_LENGTH] = '\0';
#ifdef LOGGING
	logToFile(logFile, "Received FIL from server for msg #: %s\n", filMsgNum);
#endif
	int i_filMsgNum = atoi(filMsgNum);
	//cout << i_filMsgNum << endl;

	char filBlockCount[MESSAGE_NUM_LENGTH+1];
	strncpy_s( filBlockCount, buff+i_ackOffset+MESSAGE_NUM_LENGTH+1, MESSAGE_NUM_LENGTH );
	//cout << "Block Count: " << filBlockCount << endl;
	int i_filBlockCount = atoi(filBlockCount);

	MessageList::iterator msgInfo = waitingForResponse.find(i_filMsgNum);
	FileInfo *fileInfo = msgInfo->second->fileInfo;
	//cout << "Buff size: " << strlen(buff) << endl;

	// If messages block count is equal to the expected blockcount then process
	// buffer contents and request next message if necessary
	if (fileInfo->blockCount == i_filBlockCount) {
		//cout << "Parsing Receive Buffer and Dumping into file." << endl;
		fileInfo->incrementBlockCount();
		int buffSize = strlen(buff);
		// Dump data segment into file
		fprintf(fileInfo->outputFile, "%s", buff+16);	
		// If this is the last packet close file handle, and notify user of
		// successful download
		if (buffSize < 528) {
			cout << "File transfer of " << fileInfo->fileName  << " complete..." << endl;
			delete fileInfo;
			waitingForResponse.erase(msgInfo);
		// Otherwise Request Next Part of the file from server
		} else {
			//cout << "Request next part of file..." << endl;
			memset(sendBuffer, '\0', SEND_MESSAGE_SIZE);
			sprintf_s(sendBuffer, "%s;6;%05d", filMsgNum, fileInfo->blockCount);
			mySock->send(sendBuffer);
		}
	} else {
		// Mismatch block count request desired block count from server.
		memset(sendBuffer, '\0', SEND_MESSAGE_SIZE);
		sprintf_s(sendBuffer, "%s;6;%05d", filMsgNum, fileInfo->blockCount);
		mySock->send(sendBuffer);
		cerr << "Received packet w/ incorrect block count from server." << endl;
	}
}

/* :: IMClient Function :: listCurrentFileTransfers
 */
void IMClient::listCurrentFileTransfers() {
	// Iterate through messages currently waiting for server response
	for (MessageList::iterator it_messages = waitingForResponse.begin();
			it_messages != waitingForResponse.end() ; it_messages++) {
		// If message is a type5 message it is in a download state
		if (it_messages->second->getMessageType() == type5) 
		{
			// Print file name and current blockcount
			cout << "File: " << it_messages->second->fileInfo->fileName << " Block Count: " <<
				it_messages->second->fileInfo->blockCount << endl;
		}
	}
}

/* :: IMClient Function :: requestStuckFileTransfer
	 Debug Function, not all the bugs are worked out of it though. Goes through
	 the list of waitingMessages and sends a request to the server for the next
	 packet. Needs to be modified to check to see if the file is actually stuck,
	 or if it should be closed due to an error message.
 */
void IMClient::requestStuckFileTransfer() {
	// Find all file transfers type 5 messages
	// Iterate through the list of messages waiting for response
	for (MessageList::iterator it_messages = waitingForResponse.begin();
			it_messages != waitingForResponse.end() ; it_messages++) {
		// If the message is a type5 messsage than send a request to server
		if (it_messages->second->getMessageType() == type5) {
			it_messages->second->fileInfo->incrementBlockCount();
			memset(sendBuffer, '\0', SEND_MESSAGE_SIZE);
			// Send Request for file block message to server
			sprintf_s(sendBuffer, "%05d;6;%05d", it_messages->second->messageNum,
					it_messages->second->fileInfo->blockCount);
			mySock->send(sendBuffer);
		}
	}
}

/* :: IMClient Function :: 
	 Returns true if the message is an ack Message
 */
bool IMClient::isAck(char* buff) const {
	static const char s_ack[] = "ack";
	static const int len = strlen(s_ack);
	return strncmp(buff, s_ack,len) ? 0 : 1;
}

/* :: IMClient Function :: 
	 Returns true if the message is an error message
 */
bool IMClient::isError(char* buff) const {
	static const char s_err[] = "Error";
	static const int len = strlen(s_err);
	return strncmp(buff, s_err,len) ? 0 : 1;
}

/* :: IMClient Function :: isFile
	 Returns true if the message is a fil message for file transfer.
 */
bool IMClient::isFile(char* buff) const {
	static const char s_file[] = "fil";
	static const int len = strlen(s_file);
	return strncmp(buff, s_file,len) ? 0 : 1;
}

/* :: IMClient Function :: isIncommingMsg
	 Returns true if the message from the server is a result of a type 2
	 message of another user.
 */
bool IMClient::isIncommingMsg(char *buff) const {
	static const char s_From[] = "From";
	static const int len = strlen(s_From);
	return strncmp(buff, s_From,len) ? 0 : 1;
}

/* :: IMClient Function :: serverHandler
	 Called in a separate thread. Accepts server messages and processes
	 information for the IM Client.
 */
void IMClient::serverHandler() {
#ifdef LOGGING
	logToFile(logFile, "Starting serverHandler function in new thread.\n");
#endif
	int buffSize = RECV_MESSAGE_SIZE;
	char buff[RECV_MESSAGE_SIZE+1];

	// While user is logged in you should continue to listen for server messages
	while(recvState) {
		memset(&buff, '\0', RECV_MESSAGE_SIZE+1);
		int amountReceived = mySock->recv(buff, &buffSize);
#ifdef LOGGING
		logToFile(logFile, "recvfrom: received %d bytes\nBuffer Contents: %s\n",
				amountReceived, buff);
#endif
		// Process received message
		processServerMessage(buff);
	}
}

/* :: IMClient Constructor :: 
	 Default Constructor for IMClient needs the host and port information to
	 connect it's internal socket to. Creates a socket and sets Client's
	 variables.
 */
IMClient::IMClient(const char *host, const char *service) : msgNumInt(20400),
	loginState(false), recvState(true) {
#ifdef PROTO_UDP // Connectionless Datagram
	mySock = new UDPSocket(host, service);
#elif // Stream
	mySock = new TCPSocket(host,service);
#endif //PROTO_UDP || Stream
	mySock->socketBind();
	// Set initial Message Number (mainly in ASCII format, which is no longer
	// needed probably should get rid of this function)
	incrementMessageNum();
}

/* :: IMClient Function :: login
	 Sends Type 1 message to the server
   Message Format: message#;1;userName
 */
void IMClient::login() {
	// Zero sendBuffer
	memset(sendBuffer, '\0', SEND_MESSAGE_SIZE);
	// Construct Login Message ( type 1 )
	sprintf_s(sendBuffer, "%s;1;%s", msgNumAsc, userName);
#ifdef LOGGING
	logToFile(logFile, "Logging into Server. Username: %s\nBufferContents: %s \n",userName, sendBuffer);
#endif
	// Add to list of messages waiting for server response
	addToWaitList(msgNumInt, type1);

	// Start ServerHandler a listener to handle incoming server messages in a
	// separate thread.
	h_serverHandler = (HANDLE)_beginthreadex( NULL, 0, startServerHandlerInThread,
			(void *) this, 0, NULL);

	// Send message to server
	sendBuff(sendBuffer);

	// Set login state; need to move this to the ack handler
	// TODO: move this function to the ack handler
	// Unlocks user interface to im client's commands
	setLoginState();
}

/* :: IMClient Function :: sendMessage (string)
	 Constructs and sends a type 2 message to the server from the specified
	 information.
 */
void IMClient::sendMessage(string* sendTo, string* message) {
	// Zero sendBuffer
	memset(sendBuffer, '\0', SEND_MESSAGE_SIZE);
	// Construct Message ( type 2 )
	sprintf_s(sendBuffer, "%s;2;%s\n%s\n%s", msgNumAsc, userName,
			(*sendTo).c_str(), (*message).c_str());
#ifdef LOGGING
	logToFile(logFile, "Sending Message to Server. From: %s, To: %s Message: %s \nBufferContents: %s \n",userName, sendTo, message, sendBuffer);
#endif
	// Add to list of messages waiting for server response
	addToWaitList(msgNumInt, type2);
	// Send message to socket to send to  server
	sendBuff(sendBuffer);
}

/* :: IMClient Function :: sendMessage (char)
	 Constructs and sends a type 2 message to the server from the specified
	 information.
 */
void IMClient::sendMessage(char* sendTo, char* message) {
	// Zero sendBuffer
	memset(sendBuffer, '\0', SEND_MESSAGE_SIZE);
	// Construct Message ( type 2 )
	sprintf_s(sendBuffer, "%s;2;%s\n%s\n%s", msgNumAsc, userName,
			sendTo, message);
#ifdef LOGGING
	logToFile(logFile, "Sending Message to Server. From: %s, To: %s Message: %s \n::BufferContents::\n%s\n",userName, sendTo, message, sendBuffer);
#endif
	// Add to list of messages waiting for server response
	addToWaitList(msgNumInt, type2);
	// Send message to socket to send to  server
	sendBuff(sendBuffer);
}

/* :: IMClient Function :: sendCustom 
	 Debug Function that sends the given string to the server
 */
void IMClient::sendCustom(string *message) {
	// Zero sendBuffer
	memset(sendBuffer, '\0', SEND_MESSAGE_SIZE);
	// Fill sendBuffer
	sprintf_s(sendBuffer, "%s", (*message).c_str());
	// Send sendBuffer to server
	mySock->send(sendBuffer);
}

/* :: IMClient Function :: 
   Sends (type 3) message to the server, to notify the server that you are
   logging out
 */
void IMClient::logoff() {
	// Zero sendBuffer
	memset(sendBuffer, '\0', SEND_MESSAGE_SIZE);
	// Construct Message ( type 3 )
	sprintf_s(sendBuffer, "%s;3;%s", msgNumAsc, userName);
#ifdef LOGGING
	logToFile(logFile, "Sending Logoff Message to Server.\nBufferContents: %s\n",sendBuffer);
#endif
	// Add to list of messages waiting for server response
	addToWaitList(msgNumInt, type3);
	// Send message to socket to send to  server
	sendBuff(sendBuffer);
}

/* Function: messageList
 */
/* :: IMClient Function :: fileList
   Requests a list of files from the server (type 4).
	 Sends user's request for a list of files that are available for downloading.
 */
void IMClient::fileList() {
	// Zero sendBuffer
	memset(sendBuffer, '\0', SEND_MESSAGE_SIZE);
	// Construct Message ( type 4 )
	sprintf_s(sendBuffer, "%s;4;%s", msgNumAsc, "fileList");
#ifdef LOGGING
	logToFile(logFile, "Getting file list from server.\nBufferContents: %s\n",sendBuffer);
#endif
	// Add to list of messages waiting for server response
	addToWaitList(msgNumInt, type4);
	// Send message to socket to send to  server
	sendBuff(sendBuffer);
}

/* :: IMClient Function :: setUsersFromWelcome
 * Simple function which extracts the users that are already logged in from the
 * welcom message.
 * Welcome Message Format:
 ack:msg#\n
 Welcome to the group, myName\n
 Members already logged in are:\n
 A list (possibly empty) of logged in users
 Example:
ack;10001
Welcome to the group, helloAll

Members already logged in are:
jrmq2
Chris
Mike
jrmq2
 */
void IMClient::setUsersFromWelcome(char *buff) {

	/*{{{*/
	/*
	string *buffInputString = string(*buff);
	char *buffInput = (char *) buffInputString->c_str();
	char *lineBreak;
	char delim[] = "\n";
	// Ignore First Few Lines...
	// char buff, delims, context
	cout << "Processing Buffer for user information..." << endl;
	lineBreak = strtok_s(buffInput, delim, NULL);
	lineBreak = strtok_s(NULL, delim, NULL);
	lineBreak = strtok_s(NULL, delim, NULL);
	cout << "List of Users: " << endl;
	while (lineBreak != NULL) {
		cout << "\t" << lineBreak << ", ";
		usersAvail.addUser(string(lineBreak));
		lineBreak = strtok_s(NULL, delim, NULL);
	}
	delete buffInputString;
	cout << endl;
	*/
/*}}}*/

	string buffInput = buff;

}


/* :: IMClient Function :: downloadFile
   Begins file download (type 5).
	 Sends user's request for specified file to server.
 */
void IMClient::downloadFile(string *name) {
	// Zero sendBuffer
	memset(sendBuffer, '\0', SEND_MESSAGE_SIZE);
	// Construct Message ( type 5 )
	sprintf_s(sendBuffer, "%s;5;%s", msgNumAsc, (*name).c_str());
#ifdef LOGGING
	logToFile(logFile, "Downloading Specified File .\nBufferContents: %s\n",sendBuffer);
#endif
	// Add to list of messages waiting for server response
	addToWaitList(msgNumInt, type5, name);
	// Send message to socket to send to  server
	sendBuff(sendBuffer);
}

/* :: IMClient Destructor ::
	 Since the program closes directly after this don't worry about closing out
	 too much stuff. Just removes the important variables and Terminates threads
	 and sockets.
 */
IMClient::~IMClient() {
	unsetRecvState();
	TerminateThread(h_serverHandler, NULL);
	CloseHandle(h_serverHandler);
	delete mySock;
}

/* Solution found @ http://www.cplusplus.com/forum/windows/41958/ */
unsigned __stdcall startServerHandlerInThread(void * arg) {
	IMClient *myClient = (IMClient *) arg;
	(*myClient).serverHandler();
	return 0;
}

#endif // IM_CLIENT
